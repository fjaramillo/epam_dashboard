var app = app || {};
app.dashboard = app.dashboard || {};

app.dashboard.init = function(e){
                
};

app.dashboard.start = function(e){

};
 

(function(app) {

    app.Service = ng.core.Injectable().Class({
        constructor: [ng.http.Http, function(http) { 
          this.http = http;
        }],
        getGraphFor: function(employee) {
          return this.http.get('/graph/'+employee).map(
            function(res) { return res.json(); }
          ).toPromise(); 
        }
    });

    app.ListTable = ng.core.Component({
      selector: 'listable',
      inputs : ['data'], 
      outputs: ['selected'],
      template: `<ul class="nav nav-pills nav-stacked well menu">
              <li *ngFor="let employee of data" [class.active]="active == employee.code" (click)="call(employee)">
                  <a href="#"><span><i class="fa fa-icon fa-user"></i> {{ employee.employee }}</span></a>
              </li>
             </ul>
            `
    })
    .Class({
      constructor: [function() {
         this.selected = new ng.core.EventEmitter();
         this.data = [];
         this.active = "";

         this.call = function(employee){
          this.selected.emit(employee.code);
          this.active = employee.code;
         }
      }]
      ,
      ngOnInit : function(){
        this.active = this.data[0].code;
        this.selected.emit(this.data[0].code);
      }

    });

    app.GraphComponent = ng.core.Component({
      selector: 'graph',
      inputs : ['data'], 
      outputs: [],
      template: '<div id="chartdiv" style="width:100%; height:500px;"></div>'
    })
    .Class({
      constructor: [function() {
      }]
      ,
      paintChart: function(){
        if (AmCharts.isReady) {
          this.createGraph();
        } else {
          AmCharts.ready(function () {
              app.GraphComponent.prototype.createGraph();
          });
        }
      },
      chartOptions: function(){
        return {
          "type": "serial",
          "theme": "light",
          "marginRight": 80,
          "marginTop": 17,
          "autoMarginOffset": 20,
          "dataProvider": this.data,
          "valueAxes": [{
              "logarithmic": true,
              "dashLength": 1,
              "guides": [{
                  "dashLength": 6,
                  "inside": true,
                  "label": "Maximum",
                  "lineAlpha": 1,
                  "value": 20,
                  "lineColor": "#ff0000"
              },{
                  "dashLength": 6,
                  "inside": true,
                  "label": "Minimum",
                  "lineAlpha": 1,
                  "value": 10,
                  "lineColor": "#00ff00"
              }],
              "position": "left"
          }],
          "graphs": [{
              "bullet": "round",
              "id": "g1",
              "bulletBorderAlpha": 1,
              "bulletColor": "#FFFFFF",
              "bulletSize": 7,
              "lineThickness": 2,
              "balloonText": "[[value]]:00",
              "useLineColorForBulletBorder": true,
              "valueField": "entrance",
              "title":"Entrance Time"
              }, {
              "bullet": "round",
              "id": "g2",
              "bulletBorderAlpha": 1,
              "bulletColor": "#FF2222",
              "bulletSize": 7,
              "lineThickness": 2,
              "balloonText": "[[value]]:00",
              "useLineColorForBulletBorder": true,
              "valueField": "exit",
              "title":"Exit Time"
              }
          ],
          "chartScrollbar": {},
          "chartCursor": {
              "valueLineEnabled": true,
              "valueLineBalloonEnabled": true,
              "valueLineAlpha": 0.5,
              "fullWidth": true,
              "cursorAlpha": 0.05
          },
          "dataDateFormat": "YYYY-MM-DD",
          "categoryField": "date",
          "categoryAxis": {
              "parseDates": true
          },
          "export": {
              "enabled": true
          }
        }
      },
      ngOnChanges: function(){
       this.paintChart();
      },
      ngOnInit : function(){
        this.paintChart();
      },
      createGraph: function(){
        if(this.data && this.data.length > 0){

          //Clean the current graph
          if(this.chart) AmCharts.clear();

          this.chart = AmCharts.makeChart("chartdiv", this.chartOptions());

          this.chart.addListener("dataUpdated", zoomChart);

          function zoomChart() {
              //chart.zoomToDates(new Date(2012, 2, 2), new Date(2012, 2, 10));
          }
        }
      }

    });

    app.AppComponent = ng.core.Component({
      selector: 'my-app',
      template:`<div class="row">
                      <div class="col-md-3 col-md-offset-1">
                        <listable (selected)="clicked($event)" [data]="listable"></listable>
                  </div>
                      <div class="col-md-8">
                        <graph  [data]="entry"></graph>
                    </div>
                </div>`,

      directives: [app.GraphComponent, app.ListTable],
      providers: [app.Service]
    })
    .Class({
      constructor: [ app.Service, function(service) {
        this.listable = app.dashboard.employees;

        this.clicked = function(employee){
            var _this = this;
            service.getGraphFor(employee)
            .then(function(data){
                _this.data = data;
                _this.entry = [];
                _this.data.forEach(function(e){
                    var date = moment(e.date);
                    var hour = date.hours();
                    var fdate = date.format("YYYY-MM-DD");
                    var index = _this.entry.findIndex(function(i){return i.code == e.employee_id+fdate})
                    if(index > -1)
                    {
                      if(_this.entry[index].entrance == 0)
                        _this.entry[index].entrance = hour;

                      if(_this.entry[index].exit == 0)
                        _this.entry[index].exit = hour;
                    }
                    else if(e.bFirst)
                      _this.entry.push({ entrance: hour, exit: 0, date: fdate, code: e.employee_id + fdate });
                    else if(e.bLast) 
                      _this.entry.push({ entrance: 0, exit: hour, date: fdate, code: e.employee_id + fdate });

                    
                });
            });
        }
      }]
    });

    app.AppModule = ng.core.NgModule({
    imports: [
      ng.platformBrowser.BrowserModule,
      ng.forms.FormsModule,
      ng.http.HttpModule
    ],
    declarations: [
      app.AppComponent,
      app.GraphComponent,
      app.ListTable
    ],
    bootstrap: [ app.AppComponent ]
    })
    .Class({
      constructor: function() {}
    });

    document.addEventListener('DOMContentLoaded', function() {
      ng.platformBrowserDynamic
        .platformBrowserDynamic()
        .bootstrapModule(app.AppModule);
    });

})(window.app || (window.app = {}));
