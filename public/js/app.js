var app = app || {};


app.generateUUID = function(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-8xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};
app.token = app.generateUUID();

app.init = function(e){

    e.target.removeEventListener(e.type, arguments.callee);
    // Initialize instances:
    app.socket = io.connect();
    var siofu = new SocketIOFileUpload(app.socket);

    // Configure the three ways that SocketIOFileUpload can read files:
    document.getElementById("upload_btn").addEventListener("click", siofu.prompt, false);
    siofu.listenOnInput(document.getElementById("upload_input"));
    siofu.listenOnDrop(document.getElementById("file_drop"));

    app.socket.emit('load',{});
    
    // Do something on upload progress:
    siofu.addEventListener("progress", function(event){
        container = document.getElementById('progress');
        uploaded = document.getElementById('upload-files');
        container.style.display = 'block';
        container.style.display = 'uploaded';
        bar = document.getElementById('progress-bar');

        var percent = event.bytesLoaded / event.file.size * 100;
        bar.style.width = percent+'%';
        console.log("File is", percent.toFixed(2), "percent loaded");
    });

    // Do something when a file is uploaded:
    siofu.addEventListener("complete", function(event){
        if(event.success){
            link = '<a href="/files/'+event.detail.name+'" class="list-group-item list-group-item-success">'+event.file.name+'</a>';
            container = document.getElementById("upload-files");
            step2 = document.getElementById("step2");
            list = document.getElementById("upload-files-list");

            container.style.display = 'block';
            step2.style.display = 'block';
            list.insertAdjacentHTML('beforeend',link);
            app.start();
        }
                
    });

    app.socket.on("process files", function(msg){
        if(msg.token === app.token){
            if(msg.data){
                app.data = msg.json;
                window.localStorage.data = JSON.stringify(app.data);
                                
                if(msg.errors.length > 0 ){
                    $("#result").append('<div class="wrapper-error" class="alert alert-danger">Errors found it</div>');
                    $("#result").append('<div class="wrapper-error" class="list-group-item list-group-item-danger">'+JSON.stringify(msg.errors)+'</div>');
                    $("#btnDashboard").addClass("hidden");
                }
                else
                    $("#btnDashboard").removeClass("hidden");
                
            }
            else
            {
                $("#process-files-list").append(msg.content);    
            }
            
        }
    });

    app.socket.on('error', function(error){
        var msg = '<div class="wrapper-error"><span class="list-group-item list-group-item-danger">'+error.message+'</span></div>'
        $("#process-files-list").append(msg); 
    })

};

app.start = function(e){
    app.socket.emit('process files', {token: app.token});
};

document.addEventListener("DOMContentLoaded", app.init , false);

