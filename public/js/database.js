(function(app) {

	app.SearchPipe = ng.core.Pipe({
      name: 'search',
      pure: false
    })
    .Class({
      constructor: function() {},
      transform: function(value, args) {
                    var filter = args.filter;
                    return filter ? value.filter(element => element.employee.toLocaleLowerCase().indexOf(filter) != -1) : value;
                  }
    });

   	app.OrderBy = ng.core.Pipe({
      name: 'orderBy',
      pure: false
    })
    .Class({
      	constructor: function() {
	      	var value=[];
	      	app.OrderBy._orderByComparator = function(a, b){
	        
		        if(a === null || typeof a === 'undefined') a = 0;
		        if(b === null || typeof b === 'undefined') b = 0;

				if((isNaN(parseFloat(a)) || !isFinite(a)) || (isNaN(parseFloat(b)) || !isFinite(b))){
					//Isn't a number so lowercase the string to properly compare
					if(String(a).toLowerCase() < String(b).toLowerCase()) return -1;
					if(String(a).toLowerCase() > String(b).toLowerCase()) return 1;
				}
				else{
					//Parse strings as numbers to compare properly
					if(parseFloat(a) < parseFloat(b)) return -1;
					if(parseFloat(a) > parseFloat(b)) return 1;
				}

				return 0; //equal each other
			}
      	},
      
      	transform: function(input, config = "+") {
            //make a copy of the input's reference
	    	this.value = [...input];
	    	var value = this.value;
	        
	        if(!Array.isArray(value)) return value;

	        if(!Array.isArray(config) || (Array.isArray(config) && config.length == 1)){
	            var propertyToCheck = !Array.isArray(config) ? config : config[0];
	            var desc = propertyToCheck.substr(0, 1) == '-';
	            
	            //Basic array
	            if(!propertyToCheck || propertyToCheck == '-' || propertyToCheck == '+'){
	                return !desc ? value.sort() : value.sort().reverse();
	            }
	            else {
	                var property = propertyToCheck.substr(0, 1) == '+' || propertyToCheck.substr(0, 1) == '-'
	                    ? propertyToCheck.substr(1)
	                    : propertyToCheck;

	                return value.sort(function(a,b){
	                    return !desc 
	                        ? app.OrderBy._orderByComparator(a[property], b[property]) 
	                        : -app.OrderBy._orderByComparator(a[property], b[property]);
	                });
	            }
	        }
	        else {
	            //Loop over property of the array in order and sort
	            return value.sort(function(a,b){
	                for(var i = 0; i < config.length; i++){
	                    var desc = config[i].substr(0, 1) == '-';
	                    var property = config[i].substr(0, 1) == '+' || config[i].substr(0, 1) == '-'
	                        ? config[i].substr(1)
	                        : config[i];

	                    var comparison = !desc 
	                        ? app.OrderBy._orderByComparator(a[property], b[property]) 
	                        : -app.OrderBy._orderByComparator(a[property], b[property]);
	                    
	                    //Don't return 0 yet in case of needing to sort by next property
	                    if(comparison != 0) return comparison;
	                }

	                return 0; //equal each other
	            });
	        }
		}	
    });

    app.ListTable = ng.core.Component({
      selector: 'listable',
      inputs : ['data'], 
      outputs: ['selected'],
      template: `<ul class="nav nav-pills nav-stacked well menu">
      				<li *ngFor="let table of data" [class.active]="active == table.value" (click)="call(table)">
			            <a href="#"><span><i class="fa fa-icon fa-table"></i> {{ table.label }}</span></a>
			        </li>
		         </ul>
        		`
  	})
    .Class({
      constructor: [function() {
      	 this.selected = new ng.core.EventEmitter();
      	 this.data = [];
      	 this.active = "";

      	 this.call = function(table){
      	 	this.selected.emit(table.value);
      	 	this.active = table.value;
      	 }
      }]
      ,
      ngOnInit : function(){
      	this.active = this.data[0].value;
      	this.selected.emit(this.data[0].value);
      }

    });


	app.EmployeesList = ng.core.Component({
      selector: 'datatable',
      inputs : ['table'], 
      template: `<div class="table datatable">
				<div class="form-inline">
					
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="glyphicon glyphicon-search pull-left"></i></div>
							<input type="text" placeholder="Employee name" class="form-control" (keyup)="updateTable(filter)" [(ngModel)]="filter">
							<div class="input-group-addon"><i class="glyphicon glyphicon-user pull-right"></i></div>
						</div>
					</div>
					
				</div>
				
				<table class="table table-hover table-striped table-sortable">
				  <thead>
				    <tr>
				      <th *ngFor="let column of table.columns" [class]="selectedClass(column.variable)" (click)="changeSorting(column.variable)">
				        {{column.display}}
				      </th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr *ngFor="let object of table.data | orderBy : convertSorting() ">
				      <td *ngFor="let column of table.columns">
				        {{object[column.variable]}}
				      </td>
				    </tr>
				  </tbody>
				</table>
				<ul class="pager" *ngIf="pager.visible">
					<li><a href="#" (click)="changePage('decrease')"><i class="fa fa-icon fa-chevron-left"></i></a></li> 
					<li *ngFor="let page of pager.pages; let i = index" [class]="pageSelected(i + 1)"><a href="#" (click)="changePage(i + 1)" > {{i + 1}} </a></li>
					<li><a href="#" (click)="changePage('increase')"><i class="fa fa-icon fa-chevron-right"></i></a></li>
				</ul>
				</div>
				`,
      pipes: [app.SearchPipe,app.OrderBy]
    })
    .Class({
      constructor: [function() {
      	this.sort = [];
      	this.pager = { visible: false, pages: [], pagination: 10 };
      	
      }],
      
      pageSelected : function(page){
  		return this.pager.current == page ? 'active' : ''
      },
      changePage : function(page){
      		if(page == "decrease"){
	       		if(this.pager.current > 1){
	       			this.pager.current--;
	       			var end = this.pager.current * this.pager.pagination;
	       			var start = end - this.pager.pagination;
	       			this.table.data = this.pager.fulldata.slice(start, end);
	       		}
      		}else if(page == "increase"){

      			if(this.pager.current < this.pager.numPages){
	       			this.pager.current++;
	       			var end = this.pager.current * this.pager.pagination;
	       			var start = end - this.pager.pagination;
	       			this.table.data = this.pager.fulldata.slice(start, end);
	       		}
      		}
      		else{
      			var nextPage = Number.parseInt(page);
      			if(!isNaN(nextPage) && this.pager.current != nextPage){
	       			this.pager.current = nextPage;
	       			var end = this.pager.current * this.pager.pagination;
	       			var start = end - this.pager.pagination;
	       			this.table.data = this.pager.fulldata.slice(start, end);
	       		}
      		}
  	  },
      ngOnChanges() {
	       console.log(this.table);
	       this.pager.fulldata = this.table.data;
	       this.updatePagination();	       
      },
      updatePagination: function(){
      	var numItems = this.table.data.length;

	       if(numItems > this.pager.pagination)
	       {
	       		this.pager.numPages = Math.ceil(numItems/this.pager.pagination);
	       		this.pager.pages = new Array(this.pager.numPages);
	       		this.table.data = this.table.data.slice(0,this.pager.pagination)
	       		this.pager.current = 1;
	       		this.pager.visible = true;
	       }
	       else{
	       		this.pager.visible = false;
	       }
      },
      updateTable: function(filter){
      	console.log(filter);
      	this.table.data = filter ? this.pager.fulldata.filter(element => element.employee.toLocaleLowerCase().indexOf(filter) != -1) : this.pager.fulldata;
      	this.pager.current = 1;
      	this.updatePagination();

      },
      selectedClass: function(columnName){
	    return columnName == this.sort.column ? 'sort-' + this.sort.descending : false;
	  },
	  changeSorting: function(columnName){
	    var sort = this.sort;
	    if (sort.column == columnName) {
	      sort.descending = !sort.descending;
	    } else {
	      sort.column = columnName;
	      sort.descending = false;
	    }
	  },
	  convertSorting: function(){
	    return this.sort.descending ? '-' + this.sort.column : this.sort.column;
	  }
    })

	app.AppComponent = ng.core.Component({
      selector: 'my-app',
      template:`<div class="row">
      				<div class="col-md-4">
      					<listable (selected)="clicked($event)" [data]="listable"></listable>
					</div>
      				<div class="col-md-8">
      					<datatable  [table]="table"></datatable>
  					</div>
				</div>`,

      directives: [app.EmployeesList, app.ListTable]
    })
    .Class({
      constructor: [ ng.core.NgZone, function(zone) {
      	this.listable = [{value:"employees", label:"Employees"}, {value:"events", label:"Events"}, {value:"summaries", label:"Summaries"}];
      	this.clicked = function(table){
            this.table = { columns: app.myapp.data.config[table], data: app.myapp.data[table]};
        }
      }]
    });

  	app.AppModule = ng.core.NgModule({
		imports: [
			ng.platformBrowser.BrowserModule,
			ng.forms.FormsModule
		],
		declarations: [
			app.AppComponent,
			app.EmployeesList,
			app.SearchPipe,
			app.OrderBy,
			app.ListTable
		],
		bootstrap: [ app.AppComponent ]
    })
    .Class({
      constructor: function() {}
    });

    document.addEventListener('DOMContentLoaded', function() {
	    ng.platformBrowserDynamic
	      .platformBrowserDynamic()
	      .bootstrapModule(app.AppModule);
  	});

})(window.app || (window.app = {}));
