var mongoose = require( 'mongoose' ), Event = mongoose.model('Event'), Employee = mongoose.model('Employee');
var Promise = require('bluebird');
var moment = require('moment');
var config = require('../config');
var winston = require('winston');
var ctrlEvent = {};

ctrlEvent.saveJSON = function(json){
	return new Promise(function(resolve, reject){
		var _nosaved = [];
		var _errors = [];
		var _saved = [];
		_this = this;

		json.forEach(function(item){
			var temp = new Event();
			
			temp.employee = item.employee;
			temp.date = ctrlEvent.formatDate(item.date);
			temp.id = item.id;

			if(temp.date !== undefined && temp.employee)
				_nosaved.push(temp);
			else{
				_this.errors.push(item);
			}
		})

	 	Promise.map(_nosaved, function(query){ 
	 		//Create function to process the save function of each event
	 		return query.save()
	 		.then(function(saved){
	 			_saved.push(saved);
	 			var employ = new Employee({ code: saved.id, employee: saved.employee });
	 			Employee.findOne({ code: saved.id})
	 			.then(function(doc){
	 				if(!doc){
	 					winston.log("debug","Employee saving with data " + employ);
	 					employ.save()
	 					.catch(function(err){
	 						winston.log("debug","Employee error saving " + err.message);
	 					});
	 				}
	 			})
	 			
	 			winston.log("debug","Event saved with data " + saved.date);
	 		})
	 		.catch(function(e){
	 			_this.errors.push(e.message);
	 		}); 
	 	}, { concurrency: 1 })
    	.then(function(){ // Everything is done, render and do whatever });
	    	resolve({events: _nosaved, saved: _saved, errors: _this.errors});
		});	
	});
};	

ctrlEvent.formatDate = function(string){
var obj = string.split(' ');
if(obj.length === 2){
	var date = obj[0].split(config.dateSeparator);
	var time = obj[1].split(config.hourSeparator);
	
	return moment(date[2]+"-"+date[0]+"-"+date[1]+" "+time[0]+":"+time[1],"YYYY-MM-DD HH:mm")
}

return new Date(string);
};

ctrlEvent.getEvents = function(query, select){

	return Event.find(query, select)
	.then(function(data){
		return data;
	})
}

module.exports = ctrlEvent;