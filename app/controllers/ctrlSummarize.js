var mongoose = require('mongoose');
var Summarize = mongoose.model('Summarize');
var Employee = mongoose.model('Employee');
var Promise = require('bluebird');
var moment = require("moment");
var winston = require('winston');
var ctrlSummarize= {};


ctrlSummarize.getEvents = function(doc){
	return new Promise(function(resolve, reject){
		
		var y = doc.date.getFullYear();
		var m = doc.date.getMonth()+1;
		var d = doc.date.getDate();
		var start = moment(y+"-"+m+"-"+d,"YYYY-MM-DD").startOf('day');
		var end = moment(start).add(1, 'days');

		var query = {"date": {"$gte": start.toDate(), "$lt":end.toDate()}, "employee_id": doc.id};

		Summarize.find(query)
	    .then(function(docs){
	   		var data = {};
	   		data.doc = doc;
	   		data.docs = docs;
	   		winston.log("debug","Found " + docs.length + " Docs for " + doc.date)
	   		resolve(data);
  	    })
  	    .catch(function(error){
  	    	reject(error)
  	    })

	})
};

ctrlSummarize.updateJSON = function(json){
	
	return new Promise(function(resolve, reject){
		Promise.reduce(json, function(count, doc){
		winston.log("debug","Calling Reducer on Summarize for Document # " + count + " with Date: " + doc.date)
		return ctrlSummarize.execute(doc).then(function(res){
			winston.log("debug","Finished item on Reducer");
			return count++;
		});
		}, 0)
		.then(function(result){
			winston.log("debug","Finished reducer with " + result);
			resolve(result);
		})
		.catch(function(error){
			winston.log("debug","Finished reducer with error " + error.message);
			reject(error);
		})
	})
};

ctrlSummarize.execute = function(doc){
	var docs = ctrlSummarize.getEvents(doc);
	var update = docs.then(ctrlSummarize.update)
	return Promise.join(docs, update, function(docs, update){
		winston.log("debug","Finished Summarize");
		winston.log("debug","Doc.date is " + docs.doc.date);
		return update;
	})
}

ctrlSummarize.update = function(data){
	var doc = data.doc;
	var docs = data.docs;

	return new Promise(function(resolve, reject){
		
		if(docs.length > 0){
			winston.log("debug","Checking existing Summarize");

			return Promise.try(function(){
				var res = {};
				docs.forEach(function(sum){
					winston.log("debug","Checking Summarize : Dates[" + sum.date + "||" + doc.date + "]");
					//If the document has a date less than current first update
					if((sum._id !== doc._id && sum.bFirst == true && sum.date > doc.date)){
						winston.log("debug","Updating Summarize");
						sum.date = doc.date;

						sum.save()
							  	.then(function(new_doc){
									res = new_doc;
								})
								.catch(function(error){
									winston.log("debug","Summarize could not update because: " + error.message);
									res.error = error;
								});
					}
					//if document has a date greater than current last update
					else if (sum._id !== doc._id && sum.bLast == true && doc.date > sum.date){
						winston.log("debug","Updating Summarize");
						sum.date = doc.date;

						sum.save()
								.then(function(new_doc){
									res = new_doc;
								})
								.catch(function(error){
									winston.log("debug","Summarize could not update because: " + error.message);
									res.error = error;
								});
					}

				})

				return res;
			})
			.then(function(doc){
				if(doc.error) reject(doc.error)

				resolve(doc);
			})
			
		}
		else //We will create the first register
		{
			
			
			var _l = [true, false]
	
			return Promise.map(_l, function(item){
				var sum = new Summarize({ date: doc.date, employee: doc.employee, employee_id: doc.id });
				sum.bFirst = item ? true : false;
				sum.bLast = item ? false : true;
				winston.log("debug","Creating New Summarize for Date " + sum.date);
				return sum.save().then(function(res){
					winston.log("debug","Created Summarize for Date " + res.date);
					return res;
				})
			})
			.then(function(items){
				resolve(items);
			})
			.catch(function(error){
				reject(error);
			})
			
		}
		
	});
};	

ctrlSummarize.getEmployees = function(query, select){
	return Employee.find(query, select)
	.then(function(data){
		return data;
	});
};

ctrlSummarize.getSummaries = function(query, select){
	return Summarize.find(query, select)
	.then(function(data){
		return data;
	});
};

ctrlSummarize.getGraphFor = function(employee){
	return Summarize.find({employee_id: employee}, "-_id date employee_id bFirst bLast")
	.sort({date: 1})
	.then(function(data){
		return data;
	});
};

module.exports = ctrlSummarize;