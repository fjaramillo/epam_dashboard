XLSX = require('xlsx');

var excel = {};

excel.processFiles = function(options, callback){
	var json = [];
	try{
		options.files.forEach(function(doc){
			// { rows:[ row:{ title: value, title1: value1 } ]}
			json.push(excel.parseFile(doc, {}));
		});
		return callback(null, json);
	}catch(e){
		return callback(e, null);
	}
};

excel.parseFile = function(path, options){
	/* Get workbook */	
	var workbook = XLSX.readFile(path);

	return excel.to_json(workbook);	
};

excel.to_json = function(workbook) {
	var result = {};
	workbook.SheetNames.forEach(function(sheetName) {
		var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
		if(roa.length > 0){
			result[sheetName] = roa;
		}
	});
	return result;
}

module.exports = excel;