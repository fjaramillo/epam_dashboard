var config = {};

// Build the connection string
config.dbURI = 'mongodb://localhost:32769/Epam?authSource=admin';
config.dbOptions = {
  user: 'epam',
  pass: 'epam'
};
config.dateSeparator = "/";
config.hourSeparator = ":";

module.exports = config;