var util = {};

util.divMessage = function(token, message){
	return { token: token, content: '<div class="list-group-item list-group-item-info">'+message+'</div>' };
};

module.exports = util;