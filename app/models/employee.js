var mongoose = require('mongoose');


var Schema = new mongoose.Schema({
  employee: String,
  code: { type: String,  index: true, unique: true }
});

module.exports = mongoose.model('Employee', Schema);

