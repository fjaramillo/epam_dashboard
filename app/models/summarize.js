var mongoose = require('mongoose');


var Schema = new mongoose.Schema({
  date: Date,
  employee: String,
  employee_id: String,
  bFirst: Boolean,
  bLast: Boolean
});

module.exports = mongoose.model('Summarize', Schema);