var express = require('express');

var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sifu = require('socketio-file-upload');
var socketio = require('socket.io');

var exphbs  = require('express-handlebars');
var http = require('http');
var fs = require('fs');
var excel = require('./app/controllers/excel');
var Promise = require('bluebird');
var db = require('./app/models/db');
var util = require('./app/utils/util');
var ctrlEvent = require('./app/controllers/ctrlEvent')
var ctrlSummarize = require('./app/controllers/ctrlSummarize');

app = express();

var routes = require('./routes/index');
var r_dashboard = require('./routes/dashboard');
var r_database = require('./routes/database');
var r_graph = require('./routes/graph');

// view engine setup
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// Use the session middleware

var winston = require('winston');
winston.level = 'debug';

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(sifu.router);

app.use('/files', express.static(__dirname + '/processed')); // 
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js')); // redirect bootstrap JS
app.use('/js', express.static(__dirname + '/node_modules/moment/min')); // redirect moment JS
app.use('/fonts', express.static(__dirname + '/node_modules/bootstrap/fonts')); // redirect bootstrap JS
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist')); // redirect JS jQuery
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css')); // redirect CSS bootstrap
app.use('/libs', express.static(__dirname + '/libs')); // redirect CSS bootstrap
app.use('/amcharts/images', express.static(__dirname + '/libs/amcharts/images')); // redirect CSS bootstrap

app.use('/', routes);
app.use('/dashboard', r_dashboard);
app.use('/database', r_database);
app.use('/graph', r_graph);

var port = normalizePort(process.env.PORT || '3001');
app.set('port', port);

app.server = http.createServer(app);


var io = socketio.listen(app.server.listen(port));
app.files = [];

io.sockets.on("connection", function(socket){

    // Make an instance of SocketIOFileUpload and listen on this socket:
    var uploader = new sifu();
    uploader.dir = "tmp/";
    uploader.listen(socket);

    // Do something when a file is saved:
    uploader.on("saved", function(event){
        ext = event.file.pathName.substring(event.file.pathName.lastIndexOf("."));
        name = Math.random().toString(36) + ext;
        newPath = __dirname + "/processed/"+name;
        

        var source = fs.createReadStream(event.file.pathName);
        var dest = fs.createWriteStream(newPath);

        source.pipe(dest); 

        event.file.clientDetail.url = newPath; 
        event.file.clientDetail.name = name;

        app.files = [newPath];
        source.on('end', function() { fs.unlink(event.file.pathName); });
        source.on('error', function(err) { 
          fs.unlink(event.file.pathName);
        });  
    });

    // Error handler:
    uploader.on("error", function(event){
        winston.log("debug","Error from uploader", event);
    });

     socket.on('load', function(msg){
        app.files = [];
     });

    socket.on('process files', function(msg){
      
      winston.log("debug",'Reading Files');

      io.emit('process files',util.divMessage(msg.token, "Processing Files..."));
      if(app.files && app.files.length > 0 )
        excel.processFiles({files: app.files}, function(error, json){
            if(error){
              io.emit('error', error);
              winston.log("debug",error);
            } 
            
            var _this = this;
            _this.sheets = [];
            _this.errors = [];
            
            json.forEach(function(file){
              Object.keys(file).forEach(function(sheetName){
                winston.log("debug","Excel-> adding sheet " + sheetName);
                _this.sheets.push(file[sheetName]);
              })
            })

            Promise.map(_this.sheets, function(sheet){ 
              winston.log("debug","Calling SaveJSON from App.js");
              return  ctrlEvent.saveJSON(sheet)
                      .then(function(events){
                          winston.log("debug","Events Saved from App.js");
                           io.emit('process files',util.divMessage(msg.token, "Saving Data on Database..."));
                          events.summarize = [];
                          var p = ctrlSummarize.updateJSON(events.saved)
                                  .then(function(data){
                                    winston.log("debug","Then from updateJSON from App.js");
                                    events.summarize.push(data)
                                    return data;
                                  })
                                  .catch(function(err){
                                    events.errors.push(err.message);
                                  })
                          return p;
                      })
                      .catch(function(er){
                        winston.log("debug","Catched Error on Save " + er.message);
                        _this.errors.push(er.message);
                      }); 
            }, { concurrency: 1 })
            .then(function(info){ 
              winston.log("debug","Everything finished on App.js");
               io.emit('process files',util.divMessage(msg.token, "Data saved on Database"));
              if(info.errors && info.errors.length > 0){
                winston.log("debug",info.errors);
                _this.errors.push(info.errors);
              } 
              var data = { data: true, json: info, token: msg.token, errors: _this.errors };
              io.emit('process files', data);      
            })
            .catch(function(error){
              winston.log("debug","Error Catched");
              var data = { data: true, json: [], token: msg.token, errors: _this.error };
              io.emit('process files', data);    
            }); 
            
        });
      else{
        var data = { data: true, json: [], token: msg.token, errors: "Not File Found!" };
        io.emit('process files', data);    
      }

    });
});



/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

module.exports = app;