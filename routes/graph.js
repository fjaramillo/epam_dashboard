var express = require('express');
var router = express.Router();
var ctrlSummarize = require('../app/controllers/ctrlSummarize');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json({ message: "API to render the Graph"});
})

router.get('/:employee', function(req, res, next) {
  	
  var employee = req.params.employee;

  console.log(employee);
  ctrlSummarize.getGraphFor(employee)
	.then(function(data){
		console.log(data.length);
		res.json(data);
	})
	.catch(function(err){
		res.json({ error: err });
	});
});

module.exports = router;
