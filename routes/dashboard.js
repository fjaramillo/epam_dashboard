var express = require('express');
var router = express.Router();
var ctrlSummarize = require('../app/controllers/ctrlSummarize');

/* GET users listing. */
router.get('/', function(req, res, next) {
  	ctrlSummarize.getEmployees()
	.then(function(data){
		console.log(data.length);
		res.render('dashboard', { scripts: '</script><script src="js/dashboard.js"></script>', 
  			styles: '<link rel="stylesheet" href="/libs/amcharts/plugins/export/export.css" type="text/css" media="all" />',
  			employees: data.length > 0 ? JSON.stringify(data) : "[]"
  		});
	})
	.catch(function(err){
		res.render('dashboard', { scripts: '</script><script src="js/dashboard.js"></script>', 
  			styles: '<link rel="stylesheet" href="/libs/amcharts/plugins/export/export.css" type="text/css" media="all" />',
  			error: err,
  			employees: []
  		});
	});
});

module.exports = router;
