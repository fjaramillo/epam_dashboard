var express = require('express');
var router = express.Router();
var ctrlSummarize = require('../app/controllers/ctrlSummarize');
var ctrlEvent = require('../app/controllers/ctrlEvent');
/* GET users listing. */
router.get('/', function(req, res, next) {
  
  var response = {};
  response.config = { 
    "employees" : [{
                    display: 'Id', //The text to display
                    variable: 'code', //The name of the key that's apart of the data array
                    filter: 'text' //The type data type of the column (number, text, date, etc.)
                  },
                  {
                    display: 'Full Name', //The text to display
                    variable: 'employee', //The name of the key that's apart of the data array
                    filter: 'text' //The type data type of the column (number, text, date, etc.)
                  }],
    "events" :    [{
                    display: 'Date', //The text to display
                    variable: 'date', //The name of the key that's apart of the data array
                    filter: 'dateTime' //The type data type of the column (number, text, date, etc.)
                  },
                  {
                    display: 'Employee', //The text to display
                    variable: 'employee', //The name of the key that's apart of the data array
                    filter: 'text' //The type data type of the column (number, text, date, etc.)
                  }],             
    "summaries" : [{
                    display: 'Date', //The text to display
                    variable: 'date', //The name of the key that's apart of the data array
                    filter: 'dateTime' //The type data type of the column (number, text, date, etc.)
                  },
                  {
                    display: 'Employee', //The text to display
                    variable: 'employee', //The name of the key that's apart of the data array
                    filter: 'text' //The type data type of the column (number, text, date, etc.)
                  },
                  {
                    display: 'In', //The text to display
                    variable: 'bFirst', //The name of the key that's apart of the data array
                    filter: 'text' //The type data type of the column (number, text, date, etc.)
                  },
                  {
                    display: 'Out', //The text to display
                    variable: 'bLast', //The name of the key that's apart of the data array
                    filter: 'text' //The type data type of the column (number, text, date, etc.)
                  }]
  }

  ctrlSummarize.getEmployees({},'-_id code employee')
  .then(function(data){
    response.employees = data;
    
    return ctrlEvent.getEvents({},"")
    .then(function(data){
      
      response.events = data;
      
      return ctrlSummarize.getSummaries({}, "")
      .then(function(data){
        
        response.summaries = data;

        console.log(JSON.stringify(response));

        res.render('database', { scripts: '</script></script><script src="js/database.js"></script>', 
          styles: '',
          data: JSON.stringify(response) 
        });

      })
      .catch(function(err){
        res.render('database', { scripts: '', 
            styles: '',
            error: err,
            employees: []
          });
      });
    })
  })
	.catch(function(err){
    res.render('database', { scripts: '', 
        styles: '',
        error: err,
        employees: []
      });
  });


  
	
});

module.exports = router;
