EPAM Time Tracker
===================

Epam time tracker is the website to control the entrance and exit of Epam employee's. The website was designed using the next technologies:

 1. NodeJS
 2. MongoDB (Mongoose)
 3. Angular 2 (Version 2.0.0-rc.5)
 4. Socket.io
 5. Express
 6. Bootstrap, FontAwesome
 7. Amcharts (Graph builder)

----------


Installation
-------------

To install Epam Time Tracker you should download the code or clone the repository, open your preferer console and run the next commands:

    npm install

To run the application you should execute this command:

    node app
    
It will start the application on the port 3001, if you are running in your local environment you could go to http://localhost:3001.

> **Note:**

> - Please check the config files located in **[root]/app/config**
> - You will need a **MongoDB server** running with credentials to create database and collections
> - You will need permisions to write in **[root]** folder, the application save every excel file into the folder **[root]/processed**

#### <i class="icon-refresh"></i> Next Stage

 - Delete excel files after save data in database
 - Add authentication
 - Allow the admin to make rollback from historical load for the database
 - Add Unit Tests
 - Add Export feature for graphs

